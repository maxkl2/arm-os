
# arm-os

A toy operating system targeted at the ARM architecture.

Developed and tested only on the ARM Cortex-A9 processor of the Xilinx Zynq-7010 SoC (specifically the Arty Z7-10 dev board) so there's a high probability it won't work on any other processor.

## Building

Requirements:
- the GNU ARM embedded toolchain (arm-none-eabi)
  - this should be in your `$PATH` so that CMake can find it
- CMake

To initialize the CMake project:
```sh
mkdir -p build/debug
cd build/debug
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=arm-cortex-a9_toolchain.cmake ../..
```

Afterwards, the kernel can be built with
```sh
make kernel
```

## Running it

I find it easiest to upload the kernel using a standalone XSCT console without using Vitis. The XSCT executable is located at `/path/to/vitis/bin/xsct`. An example session:
```
****** Xilinx Software Commandline Tool (XSCT) v2019.2
  **** SW Build 2708876 on Wed Nov  6 21:39:14 MST 2019
    ** Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.


xsct% connect                                                                                                                                                            
tcfchan#0                                                                                                                                                                
xsct% targets                                                                                                                                                            
  1  APU
     2  ARM Cortex-A9 MPCore #0 (Running)
     3  ARM Cortex-A9 MPCore #1 (Running)
  4  xc7z010
xsct% targets 2                                                                                                                                                          
xsct% rst                                                                                                                                                                
xsct% source ps7_init.tcl                                                                                                                                                
xsct% ps7_init                                                                                                                                                           
xsct% ps7_post_config                                                                                                                                                    
xsct% dow build/debug/bin/kernel.elf                                                                                                                                     
Downloading Program -- /home/max/Entwicklung/c/arm-os/build/debug/bin/kernel.elf                                                                                         
	section, .text.vector_table: 0x00000000 - 0x00000027
	section, .text: 0x00100000 - 0x00100033
100%    0MB   0.0MB/s  00:00    
Setting PC to Program Start Address 0x00000000
Successfully downloaded /home/max/Entwicklung/c/arm-os/build/debug/bin/kernel.elf
xsct% Info: ARM Cortex-A9 MPCore #0 (target 2) Stopped at 0xffffff28 (Suspended)                                                                                         
_vector_table() at exception_vectors.S: 6
6: 	b _boot // Reset
xsct% con                                                                                                                                                                
Info: ARM Cortex-A9 MPCore #0 (target 2) Running
xsct%
```

The `ps7_init.tcl` file is necessary to initialize the PS properly (PLLs, DDR memory interface, ...). The first-stage bootloader (FSBL) would do that normally but it doesn't run when debugging the CPU over JTAG. Extract it from the `.xsa` file (which is just a ZIP file) you get from the `'Export Hardware...'` dialog in Vivado.

Also see https://www.xilinx.com/html_docs/xilinx2020_1/vitis_doc/xsctusecases.html#zlc1543754646166 for more XSCT examples.
