# To-Do

- compare with bare minimum boot code from 3ap/zybo-z7-baremetal
- set linker script only for kernel and add LINK_DEPENDS property
  - https://stackoverflow.com/a/42138375/3593126
- no separate stacks for each exception, transfer to SVC mode immediately on exception entry
- review stack sizes: isn't kernel running mostly in supervisor?
- switch to SVC mode early in boot sequence
- C init
- MMU
- stack smash protection
- UART console
- user space
- system calls
- drivers
- write own FSBL
- ...
- put xsct commands into script and/or cmake command
- simplify cmake project creation
  - command line is: `cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=arm-cortex-a9_toolchain.cmake ../..`

## `mrc` arguments

```asm
mrc copr, opc1, Rd, CRn, CRm{, opc2}
// e.g.
mrc p15,  0,    r1, c0,  c0,   5
```
