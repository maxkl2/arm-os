
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "arm.h"
#include "armv7-mmu.h"
#include "init.h"
#include "asm-macros.h"

// The exception handlers store the program counter, the SPSR and r0 on the stack of the respective mode.
// Afterwards they switch to SVC mode, so we only need very small stacks for the exception modes.
struct exception_stacks {
    uint32_t irq_stack[3];
    uint32_t abt_stack[3];
    uint32_t und_stack[3];
    uint32_t fiq_stack[3];
};
struct exception_stacks exception_stacks;

void exception_stacks_init() {
    __asm__ (
        "msr cpsr_c, %[psr_irq]\n"
        "add sp, %[base], %[offset_irq]\n"
        "msr cpsr_c, %[psr_abt]\n"
        "add sp, %[base], %[offset_abt]\n"
        "msr cpsr_c, %[psr_und]\n"
        "add sp, %[base], %[offset_und]\n"
        "msr cpsr_c, %[psr_fiq]\n"
        "add sp, %[base], %[offset_fiq]\n"
        "msr cpsr_c, %[psr_svc]"
        ::
              [base] "r" (&exception_stacks),
           [psr_irq] "I" (PSR_I | PSR_F | MODE_IRQ),
        [offset_irq] "I" (offsetof(struct exception_stacks, irq_stack)),
           [psr_abt] "I" (PSR_I | PSR_F | MODE_ABT),
        [offset_abt] "I" (offsetof(struct exception_stacks, abt_stack)),
           [psr_und] "I" (PSR_I | PSR_F | MODE_UND),
        [offset_und] "I" (offsetof(struct exception_stacks, und_stack)),
           [psr_fiq] "I" (PSR_I | PSR_F | MODE_FIQ),
        [offset_fiq] "I" (offsetof(struct exception_stacks, fiq_stack)),
           [psr_svc] "I" (PSR_I | PSR_F | MODE_SVC)
    );
}

void cpu_init() {
    exception_stacks_init();
}

// TODO: volatile?
#define MMU_TABLE_SIZE 4096
extern uint32_t mmu_table[MMU_TABLE_SIZE];

#define PAGE_TABLE_SIZE 256

#define PAGE_SHIFT 12
#define PAGE_SIZE (1 << PAGE_SHIFT)
#define PAGE_MASK (PAGE_SIZE - 1)
#define SECTION_SHIFT 20
#define SECTION_SIZE (1 << SECTION_SHIFT)
#define SECTION_MASK (SECTION_SIZE - 1)

#define LOAD_OFFSET 0xc0000000

uint32_t address_phys_to_virt(uint32_t paddr) {
    return paddr + LOAD_OFFSET;
}

uint32_t address_virt_to_phys(uint32_t vaddr) {
    return vaddr - LOAD_OFFSET;
}

void mmu_unmap_section(uint32_t vaddr) {
    uint32_t section = vaddr >> SECTION_SHIFT;

    // TODO: deallocate page table

    mmu_table[section] = 0;
}

struct mmu_mapping_flags {
    uint8_t mem_attrs;
    bool execute_never;
    uint8_t domain;
    uint8_t access_permissions;
    bool shareable;
    bool not_global;
};

void mmu_map_section(uint32_t vaddr, uint32_t paddr, const struct mmu_mapping_flags *flags) {
    uint32_t section_virt = vaddr >> SECTION_SHIFT;
    uint32_t section_phys = paddr >> SECTION_SHIFT;

//    uint32_t boot_flags =
//    //    PTE_SECTION |
//        PTE_SECTION_MEM(PTE_MEM_CACHEABLE(PTE_CACHE_WBWA, PTE_CACHE_WBWA)) |
//        PTE_SECTION_DOMAIN(DOMAIN_KERNEL) |
//        PTE_SECTION_AP(PTE_AP_RW) |
//        PTE_SECTION_S;

//    struct mmu_mapping_flags flags = {
//        .mem_attrs = PTE_MEM_CACHEABLE(PTE_CACHE_WBWA, PTE_CACHE_WBWA),
//        .execute_never = false,
//        .domain = DOMAIN_KERNEL,
//        .access_permissions = PTE_AP_RW,
//        .shareable = true,
//        .not_global = false
//    };

//    uint32_t large_page_flags =
//        PTE_L2_LARGE_MEM(flags->mem_attrs) |
//        (flags->execute_never ? PTE_L2_LARGE_XN : 0) |
//        //PTE_L2_SMALL_DOMAIN(flags->domain) |
//        PTE_L2_AP(flags->access_permissions) |
//        (flags->shareable ? PTE_L2_S : 0) |
//        (flags->not_global ? PTE_L2_nG : 0);
//
//    uint32_t small_page_flags =
//        PTE_L2_SMALL_MEM(flags->mem_attrs) |
//        (flags->execute_never ? PTE_L2_SMALL_XN : 0) |
//        //PTE_L2_SMALL_DOMAIN(flags->domain) |
//        PTE_L2_AP(flags->access_permissions) |
//        (flags->shareable ? PTE_L2_S : 0) |
//        (flags->not_global ? PTE_L2_nG : 0);

//    uint32_t entry = PTE_SECTION;
//    entry |= PTE_MEM_CACHEABLE(PTE_CACHE_WBWA, PTE_CACHE_WBWA);
//    entry |= PTE_SECTION_DOMAIN(DOMAIN_KERNEL);
//    entry |= PTE_AP_RW;
//    entry |= PTE_SECTION_S;
//    entry |= section_phys << SECTION_SHIFT;

    uint32_t old_entry = mmu_table[section_virt];
    uint32_t old_type = old_entry & PTE_TYPE_MASK;
    if (old_type == PTE_PAGE_TABLE) {
        // TODO: deallocate page table
        //  but only if it was created by one of these functions!
    }

    uint32_t section_flags =
        PTE_SECTION_MEM(flags->mem_attrs)
        | (flags->execute_never ? PTE_SECTION_XN : 0)
        | PTE_SECTION_DOMAIN(flags->domain)
        | PTE_SECTION_AP(flags->access_permissions)
        | (flags->shareable ? PTE_SECTION_S : 0)
        | (flags->not_global ? PTE_SECTION_nG : 0);
    uint32_t new_entry = PTE_SECTION | section_flags | (section_phys << SECTION_SHIFT);

    mmu_table[section_virt] = new_entry;
}

void mmu_unmap_page(uint32_t vaddr) {
    uint32_t section_virt = vaddr >> SECTION_SHIFT;
    uint32_t section_entry = mmu_table[section_virt];
    uint32_t type = section_entry & PTE_TYPE_MASK;
    if (type == PTE_PAGE_TABLE) {
        uint32_t page_table_addr_phys = section_entry & 0xfffffc00;
        uint32_t page_table_addr_virt = address_phys_to_virt(page_table_addr_phys);
        uint32_t *page_table = (uint32_t *) page_table_addr_virt;

        uint32_t index_all = vaddr >> PAGE_SHIFT;
        uint32_t index = index_all % 256;

        page_table[index] = 0;

        // TODO: deallocate page table if all 256 pages are unmapped?
    } else {
        // TODO: error
    }
}

void mmu_map_page(uint32_t vaddr, uint32_t paddr, uint32_t flags) {
    uint32_t section_virt = vaddr >> SECTION_SHIFT;
    uint32_t section_entry = mmu_table[section_virt];
    uint32_t type = section_entry & PTE_TYPE_MASK;
    uint32_t *page_table;
    if (type == PTE_PAGE_TABLE) {
        uint32_t page_table_addr_phys = section_entry & 0xfffffc00;
        uint32_t page_table_addr_virt = address_phys_to_virt(page_table_addr_phys);
        page_table = (uint32_t *) page_table_addr_virt;
    } else if (type == PTE_INVALID) {
        // TODO: create page table
    } else {
        // TODO: error
    }

    uint32_t index_all = vaddr >> PAGE_SHIFT;
    uint32_t index = index_all % 256;

    uint32_t entry = 0;
    // TODO: set entry (different flags than L1 table!)
    // TODO: domain must be the same as that of the section
    page_table[index] = entry;
}

void mmu_unmap_range(uint32_t vaddr_start, uint32_t vaddr_end, uint32_t paddr_start) {
    // TODO
}

void mmu_map_range(uint32_t vaddr_start, uint32_t vaddr_end, uint32_t paddr_start, uint32_t flags) {
    // TODO
}

// TODO: custom type for flags that's mapped to either L1 or L2 flags

void mmu_table_init() {
    // TODO: unmap kernel identity mapping

    // TODO: map kernel high mapping properly
    struct mmu_mapping_flags flags = {
        .mem_attrs = PTE_MEM_CACHEABLE(PTE_CACHE_WBWA, PTE_CACHE_WBWA),
        .execute_never = false,
        .domain = DOMAIN_KERNEL,
        .access_permissions = PTE_AP_RW,
        .shareable = true,
        .not_global = false
    };

    // TODO: map vector page(s)

    // TODO: flush TLB (something else?)
}

int kernel_main() {
    cpu_init();
    mmu_table_init();

    // TODO: proper memory map (including vector table)
    // TODO: enable interrupts

    volatile int i = 0;
    while (1) {
        i++;
    }

	return 0;
}
