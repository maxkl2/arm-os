
#pragma once

#define MODE_MASK 0x1f
#define MODE_USR 0x10
#define MODE_FIQ 0x11
#define MODE_IRQ 0x12
#define MODE_SVC 0x13
#define MODE_ABT 0x17
#define MODE_UND 0x1b
#define MODE_SYS 0x1f

#define PSR_T (1u << 5)
#define PSR_F (1u << 6)
#define PSR_I (1u << 7)
#define PSR_A (1u << 8)
#define PSR_E (1u << 9)
