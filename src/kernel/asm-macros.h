
#pragma once

#ifdef __ASSEMBLER__
#define _AU(v) v
#else
#define _AU(v) v##u
#endif
