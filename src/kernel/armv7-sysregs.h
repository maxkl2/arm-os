
#pragma once

// MMU enable
#define SCTLR_M (1 << 0)
// Alignment check enable
#define SCTLR_A (1 << 1)
// Cache enable
#define SCTLR_C (1 << 2)
// CP15 barrier enable
#define SCTLR_CP15BEN (1 << 5)
// BE-32 mod enable (RAZ/SBZP)
#define SCTLR_B (1 << 7)
// SWP and SWPB enable
#define SCTLR_SW (1 << 10)
// Branch prediction enable
#define SCTLR_Z (1 << 11)
// Instruction cache enable
#define SCTLR_I (1 << 12)
// Exception vectors location
#define SCTLR_V (1 << 13)
// Round robin select
#define SCTLR_RR (1 << 14)
// Hardware access flag enable
#define SCTLR_HA (1 << 17)
// Write permission implies XN
#define SCTLR_WXN (1 << 19)
// Unpriviledged write permission implies PL1 XN
#define SCTLR_UWXN (1 << 20)
// Fast interrupts configuration enable
#define SCTLR_FI (1 << 21)
// Unaligned access support (RAO/SBOP)
#define SCTLR_U (1 << 22)
// Interrupt vectors enable
#define SCTLR_VE (1 << 24)
// Exception endianness
#define SCTLR_EE (1 << 25)
// Non-maskable FIQ support (read-only)
#define SCTLR_NMFI (1 << 27)
// TEX remap enable
#define SCTLR_TRE (1 << 28)
// Access flag enable
#define SCTLR_AFE (1 << 29)
// Thumb exception enable
#define SCTLR_TE (1 << 30)

#define ACTLR_FW 0x1
#define ACTLR_SMP 0x40

#define TTB_IRGN(v) (((v) & 0x1) << 6 | (v) >> 1)
#define TTB_S 0x2
#define TTB_RGN(v) ((v) << 3)
#define TTB_NOS 0x20

#define DACR_DOMAIN(dom, perm) ((perm) << (2 * (dom)))
#define DACR_PERM_NOACCESS 0
#define DACR_PERM_CLIENT 1
#define DACR_PERM_MANAGER 3
