
#pragma once

#define PTE_TYPE_MASK 0x3
#define PTE_INVALID 0x0
#define PTE_PAGE_TABLE 0x1
#define PTE_SECTION 0x2
#define PTE_SECTION_B (1 << 2)
#define PTE_SECTION_C (1 << 3)
#define PTE_SECTION_XN (1 << 4)
#define PTE_SECTION_DOMAIN(dom) ((dom) << 5)
#define PTE_SECTION_AP(ap) (((ap) & 0x3) << 10 | ((ap) & 0x4) << 13)
#define PTE_SECTION_TEX_SHIFT 12
#define PTE_SECTION_TEX(tex) ((tex) << PTE_SECTION_TEX_SHIFT)
#define PTE_SECTION_S (1 << 16)
#define PTE_SECTION_nG (1 << 17)
#define PTE_SECTION_NS (1 << 19)

#define PTE_L2_TYPE_MASK 0x3
#define PTE_L2_INVALID 0x0
#define PTE_L2_LARGE_PAGE 0x1
#define PTE_L2_SMALL_PAGE 0x2
#define PTE_L2_B (1 << 2)
#define PTE_L2_C (1 << 3)
#define PTE_L2_LARGE_XN (1 << 15)
#define PTE_L2_SMALL_XN (1 << 0)
#define PTE_L2_AP(ap) (((ap) & 0x3) << 4 | ((ap) & 0x4) << 9)
#define PTE_L2_LARGE_TEX_SHIFT 12
#define PTE_L2_LARGE_TEX(tex) ((tex) << PTE_L2_LARGE_TEX_SHIFT)
#define PTE_L2_SMALL_TEX_SHIFT 6
#define PTE_L2_SMALL_TEX(tex) ((tex) << PTE_L2_SMALL_TEX_SHIFT)
#define PTE_L2_S (1 << 10)
#define PTE_L2_nG (1 << 11)

// Page access permissions
// No access
#define PTE_AP_NONE 0x0
// PL1: read/write, PL0: no access
#define PTE_AP_PL1RW 0x1
// PL1: read/write, PL0: read-only
#define PTE_AP_PL1RW_PL0RO 0x2
// PL1: read/write, PL0: read/write
#define PTE_AP_RW 0x3
// PL1: read-only, PL0: no access
#define PTE_AP_PL1RO 0x5
// PL1: read-only, PL0: read-only
#define PTE_AP_RO 0x7

// Page access permissions
// No access
#define PTE_AP_NONE_OLD 0x0
// PL1: read/write, PL0: no access
#define PTE_AP_PL1RW_OLD PTE_SECTION_AP(0x1)
// PL1: read/write, PL0: read-only
#define PTE_AP_PL1RW_PL0RO_OLD PTE_SECTION_AP(0x2)
// PL1: read/write, PL0: read/write
#define PTE_AP_RW_OLD PTE_SECTION_AP(0x3)
// PL1: read-only, PL0: no access
#define PTE_AP_PL1RO_OLD PTE_SECTION_AP(0x5)
// PL1: read-only, PL0: read-only
#define PTE_AP_RO_OLD PTE_SECTION_AP(0x7)

// Page memory region attributes (without TEX remap)

#define _PTE_MEM(attrs, b_bit, c_bit, tex_shift) ((((attrs) & 0x1) ? (b_bit) : 0) | (((attrs) & 0x2) ? (c_bit) : 0) | (((attrs) >> 2) << tex_shift))
//#define PTE_SECTION_MEM(attrs) ((((attrs) & 0x1) ? PTE_SECTION_B : 0) | (((attrs) & 0x2) ? PTE_SECTION_C : 0) | PTE_SECTION_TEX((attrs) >> 2))
#define PTE_SECTION_MEM(attrs) _PTE_MEM(attrs, PTE_SECTION_B, PTE_SECTION_C, PTE_SECTION_TEX_SHIFT)
#define PTE_L2_LARGE_MEM(attrs) _PTE_MEM(attrs, PTE_L2_B, PTE_L2_C, PTE_L2_LARGE_TEX_SHIFT)
#define PTE_L2_SMALL_MEM(attrs) _PTE_MEM(attrs, PTE_L2_B, PTE_L2_C, PTE_L2_SMALL_TEX_SHIFT)

#define _PTE_MEM_ATTR(tex, c, b) ((b) | ((c) << 1) | ((tex) << 2))
// Strongly-ordered
#define PTE_MEM_STRONGLY_ORDERED _PTE_MEM_ATTR(0x0, 0, 0)
// Shareable Device
#define PTE_MEM_SHAREABLE_DEV _PTE_MEM_ATTR(0x0, 0, 1)
// Outer and Inner Write-Through, no Write-Allocate
#define PTE_MEM_WT _PTE_MEM_ATTR(0x0, 1, 0)
// Outer and Inner Write-Back, no Write-Allocate
#define PTE_MEM_WB _PTE_MEM_ATTR(0x0, 1, 1)
// Outer and Inner Non-cacheable
#define PTE_MEM_NC _PTE_MEM_ATTR(0x1, 0, 0)
// Outer and Inner Write-Back, Write-Allocate
#define PTE_MEM_WBWA _PTE_MEM_ATTR(0x1, 1, 1)
// Non-shareable Device
#define PTE_MEM_NONSHAREABLE_DEV _PTE_MEM_ATTR(0x2, 0, 0)
// Cacheable memory with separate attributes for inner and outer
#define PTE_MEM_CACHEABLE(inner, outer) _PTE_MEM_ATTR(0x4 | (outer), (inner) >> 1, (inner) & 0x1)
// Cache attribute: Non-cacheable
#define PTE_CACHE_NC 0x0
// Cache attribute: Write-Back, Write-Allocate
#define PTE_CACHE_WBWA 0x1
// Cache attribute: Write-Through, no Write-Allocate
#define PTE_CACHE_WT 0x2
// Cache attribute: Write-Back, no Write-Allocate
#define PTE_CACHE_WB 0x3

// Strongly-ordered
#define PTE_MEM_STRONGLY_ORDERED_OLD PTE_SECTION_TEX(0x0)
// Shareable Device
#define PTE_MEM_SHAREABLE_DEV_OLD (PTE_SECTION_TEX(0x0) | PTE_SECTION_B)
// Outer and Inner Write-Through, no Write-Allocate
#define PTE_MEM_WT_OLD (PTE_SECTION_TEX(0x0) | PTE_SECTION_C)
// Outer and Inner Write-Back, no Write-Allocate
#define PTE_MEM_WB_OLD (PTE_SECTION_TEX(0x0) | PTE_SECTION_C | PTE_SECTION_B)
// Outer and Inner Non-cacheable
#define PTE_MEM_NC_OLD PTE_SECTION_TEX(0x1)
// Outer and Inner Write-Back, Write-Allocate
#define PTE_MEM_WBWA_OLD (PTE_SECTION_TEX(0x1) | PTE_SECTION_C | PTE_SECTION_B)
// Non-shareable Device
#define PTE_MEM_NONSHAREABLE_DEV_OLD PTE_SECTION_TEX(0x2)
// Cacheable memory with separate attributes for inner and outer
#define PTE_MEM_CACHEABLE_OLD(inner, outer) (PTE_SECTION_TEX(0x4 | (outer)) | (inner) << 2)
//// Cache attribute: Non-cacheable
//#define PTE_CACHE_NC 0x0
//// Cache attribute: Write-Back, Write-Allocate
//#define PTE_CACHE_WBWA 0x1
//// Cache attribute: Write-Through, no Write-Allocate
//#define PTE_CACHE_WT 0x2
//// Cache attribute: Write-Back, no Write-Allocate
//#define PTE_CACHE_WB 0x3

// TODO: fix all references to these macros (boot.S, )
// TODO: remove assertions

#include <assert.h>

static_assert(PTE_SECTION_AP(PTE_AP_NONE) == PTE_AP_NONE_OLD, "");
static_assert(PTE_SECTION_AP(PTE_AP_PL1RW) == PTE_AP_PL1RW_OLD, "");
static_assert(PTE_SECTION_AP(PTE_AP_PL1RW_PL0RO) == PTE_AP_PL1RW_PL0RO_OLD, "");
static_assert(PTE_SECTION_AP(PTE_AP_RW) == PTE_AP_RW_OLD, "");
static_assert(PTE_SECTION_AP(PTE_AP_PL1RO) == PTE_AP_PL1RO_OLD, "");
static_assert(PTE_SECTION_AP(PTE_AP_RO) == PTE_AP_RO_OLD, "");

static_assert(PTE_SECTION_MEM(PTE_MEM_STRONGLY_ORDERED) == PTE_MEM_STRONGLY_ORDERED_OLD, "");
static_assert(PTE_SECTION_MEM(PTE_MEM_SHAREABLE_DEV) == PTE_MEM_SHAREABLE_DEV_OLD, "");
static_assert(PTE_SECTION_MEM(PTE_MEM_WT) == PTE_MEM_WT_OLD, "");
static_assert(PTE_SECTION_MEM(PTE_MEM_WB) == PTE_MEM_WB_OLD, "");
static_assert(PTE_SECTION_MEM(PTE_MEM_NC) == PTE_MEM_NC_OLD, "");
static_assert(PTE_SECTION_MEM(PTE_MEM_WBWA) == PTE_MEM_WBWA_OLD, "");
static_assert(PTE_SECTION_MEM(PTE_MEM_NONSHAREABLE_DEV) == PTE_MEM_NONSHAREABLE_DEV_OLD, "");
static_assert(PTE_SECTION_MEM(PTE_MEM_CACHEABLE(PTE_CACHE_NC, PTE_CACHE_WBWA)) == PTE_MEM_CACHEABLE_OLD(PTE_CACHE_NC, PTE_CACHE_WBWA), "");
static_assert(PTE_SECTION_MEM(PTE_MEM_CACHEABLE(PTE_CACHE_WB, PTE_CACHE_WT)) == PTE_MEM_CACHEABLE_OLD(PTE_CACHE_WB, PTE_CACHE_WT), "");
