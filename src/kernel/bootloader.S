
#define SCU_BASE_ADDR 0xf8f00000
// SCU Invalidate All Registers in Secure State Register
#define SCU_IARSSR_ADDR (SCU_BASE_ADDR + 0x0c)

/*    // TODO: necessary?
    // Set VBAR (Vector Base Address Register) to the address of the vector table
    adr r0, _vector_table
    mcr p15, 0, r0, c12, c0, 0

    // Invalidate SCU tag RAMs of all cores
    ldr r7, =SCU_IARSSR_ADDR
    ldr r6, =0x0000ffff
    str r6, [r7]

    // TODO: this should not be necessary, it's done by the bootloader
    mov r0, #0
    // TLBIALL, Invalidate entire unified TLB
    mcr p15, 0, r0, c8, c7, 0
    // ICIALLU, Invalidate all instruction caches to PoU
    mcr p15, 0, r0, c7, c5, 0
    // BPIALL, Invalidate all entries from branch predictors
    mcr p15, 0, r0, c7, c5, 6
    bl invalidate_dcache

    // TODO: is already disabled
    // Clear MMU enable bit in SCTLR
    mrc p15, 0, r0, c1, c0, 0
    bic r0, #0x1
    mcr p15, 0, r0, c1, c0, 0

    // TODO: set up exception stacks later
    mrs r0, cpsr
    mvn r1, #0x1f
    and r0, r1
    orr r0, #0x12
    msr cpsr, r0
    ldr sp, =_irq_stack
    bic r0, #0x200
    msr spsr_fsxc, r0
*/
    // TODO: map whole OCM high, should ideally be done by the bootloader

//invalidate_dcache:
//1:  b 1b
//    bx lr